/*
 Descr: 
 This sql script creates a quality point system for each jupiter borehole
 and estimates the combined value of the individual borehole.
 Furthermore the script evaluates if a borehole registration is adviced, 
 divided with respec to either geological modelleling or groundwater synchronous bearing.
 
 Author: Simon Makwarth simak@mst.dk
 Licence: GNU General Public License version 3
 */
--BEGIN;

	DROP VIEW IF EXISTS mstjupiter.mstmvw_borehole_registation_quality;
	
	--CREATE VIEW mstjupiter.mstmvw_borehole_registation_quality AS (
		WITH quality_point AS (
			SELECT distinct
				b.boreholeno,
				-- Is borehole drilled for oil exploration (dapco) => inferior quality
				CASE 
					WHEN b.purpose = 'H' 
						THEN 0
					ELSE 1
					END AS not_dapco,
				-- does the borehole contain a defined filter depth-interval => superior quality
				CASE 
					WHEN boreholeid IN (
							SELECT DISTINCT 
								boreholeid 
							FROM jupiter.screen s
							WHERE s.top IS NOT NULL 
								AND s.bottom IS NOT  NULL 
						) THEN 1
					ELSE 0
					END AS has_intake,
				-- is borehole lithology description not empty, not unknown (u), not concrete (br�nd - b) => superior quality
				CASE
					WHEN b.boreholeid IN (
							SELECT DISTINCT 
								boreholeid 
							FROM jupiter.lithsamp l 
							WHERE l.rocksymbol IS NOT NULL 
								AND l.rocksymbol NOT IN ('u', 'b')		
						) THEN 1
					ELSE 0
					END AS has_litho,
				-- is the litho descr. of the borehole based on soilsamples => superior quality
				CASE 
					WHEN b.boreholeid IN (
							SELECT DISTINCT 
								boreholeid 
							FROM jupiter.lithsamp ls
							WHERE char_length(ls.rocksymbol) >= 2 
						) THEN 1
					ELSE 0
					END AS has_soilsamp,
				-- does the borehole contain chemical analysis of groundwatersamples => superior quality
				CASE 
					WHEN b.boreholeid IN (
							SELECT DISTINCT 
								gcs.boreholeid 
							FROM jupiter.grwchemsample gcs
							INNER JOIN jupiter.grwchemanalysis gca ON gcs.sampleid = gca.sampleid 
							WHERE gca.amount IS NOT NULL 
						) THEN 1
					ELSE 0
					END AS has_chemsamp,
				-- does the borehole contain measurements of the waterlevel => superior quality
				CASE 
					WHEN b.boreholeid IN (
							SELECT DISTINCT
								w.boreholeid 
							FROM jupiter.watlevel w
							WHERE w.waterlevel IS NOT NULL 
						) THEN 1
					ELSE 0
					END AS has_waterlevel,
				-- does the borehole contain a filter deeper than 5 m belov surface => superior quality
				CASE 
					WHEN b.boreholeid IN (
							SELECT DISTINCT 
								s.boreholeid 
							FROM jupiter.screen s
							WHERE bottom >= 5
						) THEN 1
					ELSE 0
					END AS intake_5mplus,
				-- does the borehole contain a geometry => superior quality
				CASE 
					WHEN b.geom IS NOT NULL
						THEN 1
					ELSE 0
					END AS has_geom,
				-- is the borehole measured using gps or a building surveyor => superior quality
				CASE
					WHEN b.locatmetho IN ('DG', 'GR', 'I', 'G')
						THEN 1
					ELSE 0
					END AS has_gps,
				-- is the borehole measured using differential gps
				CASE
					WHEN b.locatmetho IN ('DG', 'GR', 'I')
						THEN 1
					ELSE 0
					END AS has_dgps,
				-- is the drilldepth or the bottom of the last litho descr. deeper than 25 m => superior quality
				CASE 
					WHEN b.boreholeid IN (
							SELECT DISTINCT 
								b.boreholeid 
							FROM jupiter.borehole b
							INNER JOIN jupiter.lithsamp ls ON b.boreholeid = ls.boreholeid 
							WHERE b.drilldepth >= 25 OR ls.bottom >= 25
						) THEN 1
					ELSE 0
					END AS depth_25plus,
				-- is the drilldepth or the bottom of the last litho descr. deeper than 50 m => superior quality
				CASE 
					WHEN b.boreholeid IN (
							SELECT DISTINCT 
								b.boreholeid 
							FROM jupiter.borehole b
							INNER JOIN jupiter.lithsamp ls ON b.boreholeid = ls.boreholeid 
							WHERE b.drilldepth >= 50 OR ls.bottom >= 50
						) THEN 1
					ELSE 0
					END AS depth_50plus,	
				-- is the drilldepth or the bottom of the last litho descr. deeper than 75 m => superior quality
				CASE 
					WHEN b.boreholeid IN (
							SELECT DISTINCT 
								b.boreholeid 
							FROM jupiter.borehole b
							INNER JOIN jupiter.lithsamp ls ON b.boreholeid = ls.boreholeid 
							WHERE b.drilldepth >= 75 OR ls.bottom >= 75
						) THEN 1
					ELSE 0
					END AS depth_75plus,
				-- is the drilldepth or the bottom of the last litho descr. deeper than 100 m => superior quality
				CASE 
					WHEN b.boreholeid IN (
							SELECT DISTINCT 
								b.boreholeid 
							FROM jupiter.borehole b
							INNER JOIN jupiter.lithsamp ls ON b.boreholeid = ls.boreholeid 
							WHERE b.drilldepth >= 100 OR ls.bottom >= 100
						) THEN 1
					ELSE 0
					END AS depth_100plus,
				CASE 
					WHEN upper(b.use) = 'S'
						THEN 1
					ELSE 0
					END AS is_demolished,
				b.geom
			FROM jupiter.borehole b 
		)
		SELECT DISTINCT 
		count(boreholeno) FROM quality_point
		
		;
			ROW_NUMBER() over () AS row_num,
			-- total score by adding up all individual quality points
			not_dapco+has_intake+has_litho+has_soilsamp+has_chemsamp+has_waterlevel+intake_5mplus+has_geom+has_gps+has_dgps+depth_25plus+depth_50plus+depth_75plus+depth_100plus AS overall_sum, 
			-- the maximum possible number of quality points
			14 AS maximum_overall_sum,
			-- boreholes not dapco, with litho descr. using soilsamp, not measured with differential-gps and not demolished => 1, Borehole registration adviced
			-- boreholes not dapco, with litho descr. using soilsamp, measured with differential-gps => 2, Borehole registration not needed
			-- Rest of the borehole => 0, Borehole registration not adviced
			CASE
				WHEN 
					not_dapco = 1
					AND has_litho = 1
					AND has_soilsamp = 1
					AND has_dgps = 0
					AND is_demolished = 0
					THEN 1
				WHEN 
					not_dapco = 1
					AND has_litho = 1
					AND has_soilsamp = 1
					AND has_dgps = 1
					THEN 2
				ELSE 0
				END AS bor_reg_geo_model,
			-- borehole with defined intake interval, not dapco, intake is below 5 meter under surface and not demolished => 1, Borehole registration adviced
			-- borehole with defined intake interval, not dapco, intake is below 5 meter under surface and not demolished => 1, Borehole registration not needed
			-- Rest of the boreholes => 0, Borehole registration not adviced
			CASE
				WHEN 
					has_intake = 1
					AND not_dapco = 1
					AND intake_5mplus = 1
					AND is_demolished = 0
					AND has_dgps = 0
					THEN 1
				WHEN 
					has_intake = 1
					AND not_dapco = 1
					AND intake_5mplus = 1
					AND has_dgps = 1
					THEN 2
				ELSE 0
				END AS bor_reg_synkr_pejl,
			qp.*
		FROM quality_point qp
	);
	
	--GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;

--COMMIT;
