BEGIN;
	DROP TABLE IF EXISTS mstjupiter.borehole_registration_quality;
	
	CREATE TABLE mstjupiter.borehole_registration_quality AS (
		SELECT 
			ROW_NUMBER() over () AS row_num,
			boreholeno, boreholeid, geom 
		FROM jupiter.borehole b 
		ORDER BY boreholeid desc
	);
	
	-- Add primary key
	ALTER TABLE mstjupiter.borehole_registration_quality 
		ADD PRIMARY KEY (boreholeid);
	
	-- Create geometry index
	CREATE INDEX borehole_registration_quality_geom_idx
		ON mstjupiter.borehole_registration_quality
	  	USING GIST (geom);

    -- does the borehole contain a defined filter depth-interval => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_intake int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_intake = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT boreholeid 
			FROM jupiter.screen s
			WHERE s.top IS NOT NULL 
				AND s.bottom IS NOT  NULL 
    	)
    ;
   
    -- is borehole lithology description not empty, not unknown (u), not well-casing (br�nd - b), not no-data (x) => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_litho int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_litho = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT boreholeid 
			FROM jupiter.lithsamp l 
			WHERE l.rocksymbol IS NOT NULL 
				AND l.rocksymbol NOT IN ('u', 'b', 'x')	
    	)
    ;
   
    -- is the litho descr. of the borehole based on soilsamples => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_soilsamp int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_soilsamp = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT boreholeid 
			FROM jupiter.lithsamp ls
			WHERE char_length(ls.rocksymbol) >= 2 
    	)
    ;

    -- does the borehole contain chemical analysis of groundwatersamples => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_chemsamp int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_chemsamp = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT gcs.boreholeid 
			FROM jupiter.grwchemsample gcs
			INNER JOIN jupiter.grwchemanalysis gca ON gcs.sampleid = gca.sampleid 
			WHERE gca.amount IS NOT NULL 
    	)
    ;
   
    -- does the borehole contain chemical analysis of groundwatersamples measured after the year 2000 => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_chemsamp_2000 int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_chemsamp_2000 = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT gcs.boreholeid 
			FROM jupiter.grwchemsample gcs
			INNER JOIN jupiter.grwchemanalysis gca ON gcs.sampleid = gca.sampleid 
			WHERE gca.amount IS NOT NULL
				AND EXTRACT(YEAR FROM gcs.sampledate) > 2000
    	)
    ;
   
    -- does the borehole contain chemical analysis of groundwatersamples with a ionbalance of +/- 5% => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_chemsamp_ionbalance int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_chemsamp_ionbalance = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT gcs.boreholeid 
			FROM jupiter.grwchemsample gcs
			INNER JOIN jupiter.mstmvw_ionbalance_all_dates ion ON ion.boreholeno = gcs.boreholeno 
				AND ion.sampleid = gcs.sampleid 
				AND ion.intakeno = gcs.intakeno 
			WHERE abs(ion.ionbalance) <= 5
    	)
    ;   
   
    -- does the borehole contain measurements of the waterlevel => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_waterlevel int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_waterlevel = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT w.boreholeid 
			FROM jupiter.watlevel w
			WHERE w.waterlevel IS NOT NULL 
    	)
    ;
   
    -- does the borehole contain a defined drilldepth => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_drilldepth int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_drilldepth = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid
			FROM jupiter.borehole b
			WHERE b.drilldepth IS NOT NULL 
    	)
    ;    
   
    -- does the borehole contain measurements of the waterlevel => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN intake_5mplus int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET intake_5mplus = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT s.boreholeid 
			FROM jupiter.screen s
			WHERE bottom >= 5
    	)
    ;
   
    -- does the borehole contain a geometry => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_geom int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_geom = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE b.geom IS NOT NULL
    	)
    ;
   
    -- is the borehole measured using gps or a building surveyor => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_gps int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_gps = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE b.locatmetho IN ('DG', 'GR', 'I', 'G')
    	)
    ;
   
    -- is the borehole measured using dgps or a building surveyor => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_dgps int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_dgps = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE b.locatmetho IN ('DG', 'GR', 'I')
    	)
    ;
   
    -- does the borehole contain elevation => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_ele int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_ele = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid
			FROM jupiter.borehole b
			WHERE b.elevation IS NOT null
    	)
    ;
   
    -- is the borehole elevation measured using dgps or leveled elevation => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_dgps_ele int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_dgps_ele = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid
			FROM jupiter.borehole b
			WHERE upper(b.elevametho) IN ('P', 'N')
    	)
    ;
   
    -- is the drilldepth or the bottom of the last litho descr. deeper than 25 m => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN depth_25plus int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET depth_25plus = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			INNER JOIN jupiter.lithsamp ls ON b.boreholeid = ls.boreholeid 
			WHERE b.drilldepth >= 25 OR ls.bottom >= 25
    	)
    ;
   
    -- is the drilldepth or the bottom of the last litho descr. deeper than 50 m => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN depth_50plus int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET depth_50plus = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			INNER JOIN jupiter.lithsamp ls ON b.boreholeid = ls.boreholeid 
			WHERE b.drilldepth >= 50 OR ls.bottom >= 50
    	)
    ;

    -- is the drilldepth or the bottom of the last litho descr. deeper than 75 m => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN depth_75plus int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET depth_75plus = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			INNER JOIN jupiter.lithsamp ls ON b.boreholeid = ls.boreholeid 
			WHERE b.drilldepth >= 75 OR ls.bottom >= 75
    	)
    ;
   
    -- is the drilldepth or the bottom of the last litho descr. deeper than 100 m => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN depth_100plus int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
   		SET depth_100plus = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			INNER JOIN jupiter.lithsamp ls ON b.boreholeid = ls.boreholeid 
			WHERE b.drilldepth >= 100 OR ls.bottom >= 100
    	)
    ;
   
   	-- Is borehole drilled for oil exploration (dapco) => inferior quality
	--ALTER TABLE mstjupiter.borehole_registration_quality
	--	ADD COLUMN not_dapco int DEFAULT 0;
	--UPDATE mstjupiter.borehole_registration_quality AS brq
    --	SET not_dapco = 1
    --	WHERE brq.boreholeid NOT IN (
	--    	SELECT DISTINCT b.boreholeid
	--    	FROM jupiter.borehole b
	--    	WHERE b.purpose = 'H' 
    --	)
   -- ;
   
    -- does the borehole contain a defined use and purpose => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_use_purpose int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_use_purpose = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE b.use IS NOT NULL 
				AND b.purpose IS NOT NULL 
    	)
    ;  
   
    -- Is borehole use or purpose proper for measuring waterlevel => superior quality
    ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN proper_watlvl int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET proper_watlvl = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('A', 'C', 'L', 'M', 'O', 'P', 'PA', 'RE', 'V', 'VH', 'VM', 'VP', 'VR', 'VV', 'H', 'R', 'U')
				OR upper(b.purpose) IN ('A', 'C', 'L', 'M', 'O', 'P', 'PA', 'RE', 'V', 'VH', 'VM', 'VP', 'VR', 'VV', 'H', 'R', 'U')
    	)
    ;
   
    -- Is borehole use or purpose proper for geomodel => superior quality
    ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN proper_geomodel int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET proper_geomodel = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('A', 'B', 'C', 'D', 'F', 'G', 'I', 'J', 'K', 'KO', 'L', 'LO', 'LS', 'M', 'N', 'O', 'P', 'PA', 'RE', 'V', 'VA', 'VD', 'VH', 'VI', 'VM', 'VP', 'VR', 'VS', 'VV', 'H', 'R', 'U')
				OR upper(b.purpose) IN ('A', 'B', 'C', 'D', 'F', 'G', 'I', 'J', 'K', 'KO', 'L', 'LO', 'LS', 'M', 'N', 'O', 'P', 'PA', 'RE', 'V', 'VA', 'VD', 'VH', 'VI', 'VM', 'VP', 'VR', 'VS', 'VV', 'H', 'R', 'U')
    	)
    ;
   
    -- Is borehole use or purpose proper for geomodel => superior quality
    ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN proper_grwchem int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET proper_grwchem = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('A', 'C', 'M', 'O', 'P', 'PA', 'RE', 'V', 'VH', 'VM', 'VP', 'VR', 'VV', 'H', 'R', 'U')
				OR upper(b.purpose) IN ('A', 'C', 'M', 'O', 'P', 'PA', 'RE', 'V', 'VH', 'VM', 'VP', 'VR', 'VV', 'H', 'R', 'U')
    	)
    ;
   
   	-- does the borehole contain lithsample per 2 meter drilldepth => superior quality
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_lith_per_2m int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_lith_per_2m = 1
    	FROM (
    		SELECT ls.boreholeid, COUNT(ls.sampleno)/b2.drilldepth AS samples_per_meter
			FROM jupiter.lithsamp ls
			LEFT JOIN jupiter.borehole b2 ON ls.boreholeid = b2.boreholeid
			WHERE b2.drilldepth IS NOT NULL OR b2.drilldepth != 0
			GROUP BY ls.boreholeid, b2.drilldepth
    	) AS numofsamp
    	WHERE 
    		brq.boreholeid = numofsamp.boreholeid
				AND numofsamp.samples_per_meter >= 2
    ;
   
   	-- is the borehole drilled with sneglebor, t�rboring, indirekte skylleboring, luftskylleboring, rotaryboring
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN has_drillmeth int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET has_drillmeth = 1
    	WHERE brq.boreholeid IN (
    		SELECT DISTINCT dm.boreholeid
			FROM jupiter.drilmeth AS dm
			WHERE upper(dm.METHOD) IN ('G', 'T', 'I', 'L', 'R')
    	)
    ;
      
    -- calculate the combined number of quality points per borehole => higher is better
  	ALTER TABLE mstjupiter.borehole_registration_quality
  		ADD COLUMN combined_value int DEFAULT 0;
  	
   	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET combined_value = brq.has_intake + brq.has_litho + brq.has_soilsamp + 
    		brq.has_chemsamp + brq.has_chemsamp_2000 + brq.has_chemsamp_ionbalance + 
    		brq.has_waterlevel + brq.has_drilldepth + brq.intake_5mplus + 
    		brq.has_geom + brq.has_gps + brq.has_ele + brq.has_dgps_ele + 
    		brq.has_dgps + brq.depth_25plus + brq.depth_50plus + 
    		brq.depth_75plus + brq.depth_100plus + brq.has_use_purpose + 
    		brq.proper_watlvl + brq.proper_geomodel + brq.proper_grwchem + 
    		brq.has_lith_per_2m + brq.has_drillmeth
    	FROM mstjupiter.borehole_registration_quality AS brq2
    	WHERE brq.boreholeid = brq2.boreholeid
    ;
   
    -- is the borehole demolished as is therefore considered impossible to locate
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN is_demolished int DEFAULT 0;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET is_demolished = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) = 'S'
				OR upper(b.purpose) = 'S'
    	)
    ;  

    -- set a quality assessment of the borehole use and purpose with regard to measuring waterlevel => higher is better
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN grade_use_wl int DEFAULT NULL;
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_wl = 0
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('VS', 'VI', 'VD', 'VA', 'S', 'N', 'MG', 'LS', 'LO', 'KO', 'K', 'J', 'I', 'G', 'F', 'D', 'B')
				OR upper(b.purpose) IN ('VS', 'VI', 'VD', 'VA', 'S', 'N', 'MG', 'LS', 'LO', 'KO', 'K', 'J', 'I', 'G', 'F', 'D', 'B')
    	);
    
    UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_wl = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('A', 'VV', 'H')
				OR upper(b.purpose) IN ('A', 'VV', 'H')
    	);
    
   	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_wl = 2
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('C', 'L', 'O', 'PA', 'VH', 'VM', 'VP', 'VR', 'R')
				OR upper(b.purpose) IN ('C', 'L', 'O', 'PA', 'VH', 'VM', 'VP', 'VR', 'R')
    	);
    
    UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_wl = 3
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('RE', 'V')
				OR upper(b.purpose) IN ('RE', 'V')
    	);
    
   	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_wl = 4
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('M', 'P', 'U')
				OR upper(b.purpose) IN ('M', 'P', 'U')
    	)
    ;
   
    -- set a quality assessment of the borehole use and purpose with regard to geomodelling => higher is better
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN grade_use_geo int DEFAULT NULL;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_geo = 0
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('S', 'MG')
				OR upper(b.purpose) IN ('S', 'MG')
    	);
    UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_geo = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('A', 'B', 'F')
				OR upper(b.purpose) IN ('A', 'B', 'F')
    	);
    
   	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_geo = 2
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('C', 'D', 'I', 'J', 'K', 'KO', 'L', 'LO', 'LS', 'N', 'O', 'H')
				OR upper(b.purpose) IN ('C', 'D', 'I', 'J', 'K', 'KO', 'L', 'LO', 'LS', 'N', 'O', 'H')
    	);
    
    UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_geo = 3
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('G', 'PA', 'VA', 'VD', 'VH', 'VI', 'VM', 'VP', 'VS', 'R')
				OR upper(b.purpose) IN ('G', 'PA', 'VA', 'VD', 'VH', 'VI', 'VM', 'VP', 'VS', 'R')
    	);
    
   	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_geo = 4
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('M', 'P', 'RE', 'V', 'VR', 'VV', 'U')
				OR upper(b.purpose) IN ('M', 'P', 'RE', 'V', 'VR', 'VV', 'U')
    	)
    ;

       -- set a quality assessment of the borehole use and purpose with regard to chemestry water analysis => higher is better
   	ALTER TABLE mstjupiter.borehole_registration_quality
		ADD COLUMN grade_use_chem int DEFAULT NULL;
	
	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_chem = 0
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('B', 'D', 'F', 'G', 'I', 'J', 'K', 'KO', 'L', 'LO', 'LS', 'MG', 'N', 'S', 'VA', 'VD', 'VI', 'VS')
				OR upper(b.purpose) IN ('B', 'D', 'F', 'G', 'I', 'J', 'K', 'KO', 'L', 'LO', 'LS', 'MG', 'N', 'S', 'VA', 'VD', 'VI', 'VS')
    	);
    
    UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_chem = 1
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('A', 'VH', 'VM', 'VP', 'H')
				OR upper(b.purpose) IN ('A', 'VH', 'VM', 'VP', 'H')
    	);
    
   	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_chem = 2
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('C', 'O', 'R')
				OR upper(b.purpose) IN ('C', 'O', 'R')
    	);
    
    UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_chem = 3
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('PA')
				OR upper(b.purpose) IN ('PA')
    	);
    
   	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET grade_use_chem = 4
    	WHERE brq.boreholeid IN (
			SELECT DISTINCT b.boreholeid 
			FROM jupiter.borehole b
			WHERE upper(b.use) IN ('M', 'P', 'RE', 'V', 'VR', 'VV', 'U')
				OR upper(b.purpose) IN ('M', 'P', 'RE', 'V', 'VR', 'VV', 'U')
    	)
    ;
   
   	-- boreholes proper use/purpose, with litho descr. using soilsamp, not measured with differential-gps and not demolished => 1, Borehole registration adviced
	-- boreholes proper use/purpose, with litho descr. using soilsamp, measured with differential-gps => 2, Borehole registration not needed
	-- Rest of the borehole => 0, Borehole registration not adviced
	ALTER TABLE mstjupiter.borehole_registration_quality
  		ADD COLUMN bor_reg_geo_model int DEFAULT 0;	
  	
   	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET bor_reg_geo_model = 1
    	WHERE 
    		has_litho = 1
    		AND has_soilsamp = 1
    		AND proper_geomodel = 1 
    		AND (has_dgps = 0 OR has_dgps_ele = 1)
    		AND is_demolished = 0;
    	
   	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET bor_reg_geo_model = 2
    	WHERE 
    		has_litho = 1
    		AND has_soilsamp = 1
    		AND proper_geomodel = 1 
    		AND has_dgps = 1
    		AND has_dgps_ele = 1
    ;

   	-- borehole with defined intake interval, proper use/purpose, intake is below 5 meter under surface and not demolished => 1, Borehole registration adviced
	-- borehole with defined intake interval, proper use/purpose, intake is below 5 meter under surface => 2, Borehole registration not needed
	-- Rest of the boreholes => 0, Borehole registration not adviced
   	ALTER TABLE mstjupiter.borehole_registration_quality
  		ADD COLUMN bor_reg_synkr_pejl int DEFAULT 0;	
  	
   	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET bor_reg_synkr_pejl = 1
    	WHERE 
			has_intake = 1
			AND proper_watlvl = 1
			AND intake_5mplus = 1
			AND is_demolished = 0
			AND (has_dgps = 0 OR has_dgps_ele = 0);
		
   	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET bor_reg_synkr_pejl = 2
    	WHERE 
			has_intake = 1
			AND proper_watlvl = 1
			AND intake_5mplus = 1
			AND has_dgps = 1
			AND has_dgps_ele = 1
    ;
   
   	-- borehole with defined intake interval, proper use/purpose, chemsamp newer than 2000, has chemsamp, chemsamp with ionbalance within 5% and bad position/elevation => 1, Borehole registration adviced
	-- borehole with defined intake interval, proper use/purpose, chemsamp newer than 2000, has chemsamp, chemsamp with ionbalance within 5% => 2, Borehole registration not needed
	-- Rest of the boreholes => 0, Borehole registration not adviced
   	ALTER TABLE mstjupiter.borehole_registration_quality
 		ADD COLUMN bor_reg_chemsamp int DEFAULT 0;	
 	
 	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET bor_reg_chemsamp = 2
    	WHERE 
			has_intake = 1
			AND proper_grwchem = 1
			AND has_dgps = 1
			AND has_dgps_ele = 1
			AND has_chemsamp_2000 = 1
			AND has_chemsamp = 1
			AND has_chemsamp_ionbalance = 1;
		
   	UPDATE mstjupiter.borehole_registration_quality AS brq
    	SET bor_reg_chemsamp = 1
    	WHERE 
			has_intake = 1
			AND proper_grwchem = 1
			AND (has_dgps = 0 OR has_dgps_ele = 0)
			AND has_chemsamp_2000 = 1
			AND has_chemsamp = 1
			AND has_chemsamp_ionbalance = 1
    ;

   GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;
  
COMMIT;
